package models

import (
	"fmt"
	"strconv"

	"time"

	"gitlab.com/hersche/go-play/config"

	"github.com/davecgh/go-spew/spew"
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/labstack/echo"
	"golang.org/x/crypto/bcrypt"
)

type Tag struct {
	gorm.Model
	id   int `gorm:"AUTO_INCREMENT"`
	Name string
}

func (tag *Tag) toJson() echo.Map {
	return echo.Map{
		"id":   tag.ID,
		"name": tag.Name,
	}
}

type UserGroup struct {
	gorm.Model
	id     int `gorm:"AUTO_INCREMENT"`
	Name   string
	public bool   `sql:"DEFAULT:false"`
	Users  []User `gorm:"many2many:user_contact_groups;"`
}

type PermissionGroup struct {
	gorm.Model
	id    int `gorm:"AUTO_INCREMENT"`
	Name  string
	Level int
	Users []User `gorm:"many2many:user_permission_groups;"`
}

type User struct {
	gorm.Model
	id    int `gorm:"AUTO_INCREMENT"`
	Name  string
	Age   int
	Email string `gorm:"type:varchar(100);unique_index"`

	AvatarUrl string `gorm:"size:255"` // set field size to 255

	BackgroundUrl string `gorm:"size:255"` // set field size to 255

	BackgroundColor  string            `gorm:"size:255"` // set field size to 255
	Role             string            `gorm:"size:255"` // set field size to 255
	Password         string            `gorm:"size:255"` // set field size to 255
	Tags             []Tag             `gorm:"many2many:user_tags;"`
	UserGroups       []UserGroup       `gorm:"many2many:user_contact_groups;"`
	PermissionGroups []PermissionGroup `gorm:"many2many:user_permission_groups;"`
}

/**
* A method which is assigned to the user-struct
* By starting with a small letter, this a method usually is not exported and therefore like private
**/
func (user *User) toJson(db *gorm.DB) echo.Map {
	//tagsQ := make([]Tag, 0)
	db.Preload("Tags").Find(&user)
	// spew.Dump(user)
	spew.Dump(user.Tags)
	return echo.Map{
		"id":    user.ID,
		"email": user.Email,
		"age":   user.Age,
		"role":  user.Role,
		"name":  user.Name,
		"tags":  tagsToJson(user.Tags),
	}
}

func tagsToJson(tags []Tag) []echo.Map {
	var restructuredData []echo.Map
	for _, tag := range tags {
		restructuredData = append(restructuredData, tag.toJson())
	}
	return restructuredData
}

func LoginUser(c echo.Context, db *gorm.DB) string {
	users := make([]User, 0) // Make struct ready for db-queries (somehow)
	u := db.Where("name = ?", c.QueryParam("name")).First(&users)
	var user User
	rows, rowsError := u.Rows()
	if rowsError != nil {
		panic("rows-fail")
	}
	rows.Next()
	u.ScanRows(rows, &user)
	var bcryptTest = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(c.QueryParam("password")))
	if bcryptTest != nil {
		return "login failes"
	}
	// Create token
	token := jwt.New(jwt.SigningMethodHS256)
	// Set claims
	// This is the information which frontend can use
	// The backend can also decode the token and get admin etc.
	claims := token.Claims.(jwt.MapClaims)
	claims["id"] = user.ID
	claims["name"] = user.Name
	claims["email"] = user.Email
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()
	// Generate encoded token and send it as response.
	// The signing string should be secret (a generated UUID          works too)

	configuration := config.GetConfig()
	t, err := token.SignedString([]byte(configuration.JWT_SECRET))
	if err != nil {
		return "tokengenerate failed"
	}
	return t
}

func CreateUser(c echo.Context, db *gorm.DB) string {
	configuration := config.GetConfig()
	fmt.Println(c.QueryParam("age"))
	var age, ageErr = strconv.Atoi(c.QueryParam("age"))
	if ageErr != nil {
		panic("what a weird age")
	}

	var bcryptResult, bcryptErr = bcrypt.GenerateFromPassword([]byte(c.QueryParam("password")), configuration.BCRYPT_STRENGTH)
	if bcryptErr != nil {
		panic("what a weird val")
	}
	user := User{Name: c.QueryParam("name"), Age: age, Email: c.QueryParam("email"), Role: c.QueryParam("role"), Password: string(bcryptResult)}
	db.Create(&user)
	return "User created"
}

func ReturnJsonUsers(db *gorm.DB) []echo.Map {
	users := make([]User, 0) // Make struct ready for db-queries (somehow)
	// u := db.First(&users) // first user
	u := db.Find(&users) // all users
	return createRestructuredData(u)
}

func AddTagToUserId(tag string, id int, db *gorm.DB) {

	users := make([]User, 0)                  // Make struct ready for db-queries (somehow)
	u := db.Where("id = ?", id).First(&users) // user by id
	rows, rowsError := u.Rows()
	if rowsError != nil {
		panic("rows-fail")
	}
	defer rows.Close()
	var user User
	rows.Next()
	u.ScanRows(rows, &user)
	rows.Close()
	fmt.Println(db.Model(&user).Association("Tags").Count())
	db.Model(&user).Association("Tags").Append(Tag{Name: tag})
}

func ReturnJsonUserByID(db *gorm.DB, searchedId int) []echo.Map {
	users := make([]User, 0) // Make struct ready for db-queries (somehow)
	// u := db.First(&users) // first user
	// u := db.Find(&users) // all users
	//u := db.Where(&User{ID: searchedId}).First(&users)
	u := db.Where("id = ?", searchedId).First(&users) // user by id
	return createRestructuredData(u)
}

func DeleteUserByID(db *gorm.DB, searchedName string) string {
	users := make([]User, 0)                          // Make struct ready for db-queries (somehow)
	db.Where("name = ?", searchedName).Delete(&users) // user by id
	return "User deleted"
}

func createRestructuredData(result *gorm.DB) []echo.Map {
	rows, rowsError := result.Rows()
	if rowsError != nil {
		panic("rows-fail")
	}
	var restructuredData []echo.Map
	for rows.Next() {
		// Parse into model-struct
		var user User
		result.ScanRows(rows, &user)
		fmt.Println("Preparing: " + user.Email + " for json")
		restructuredData = append(restructuredData, user.toJson(result))
	}
	return restructuredData
}
