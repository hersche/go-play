package main

import (
	"fmt"

	"gitlab.com/hersche/go-play/models"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func main() {
	fmt.Println("Start command")
	db, err := gorm.Open("sqlite3", "test.db")
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()
	fmt.Println("Start migration")
	db.AutoMigrate(&models.User{})
	db.AutoMigrate(&models.Tag{})

	db.AutoMigrate(&models.PermissionGroup{})
	db.AutoMigrate(&models.UserGroup{})

	fmt.Println("Start seeding")
	db.Create(&models.User{Name: "geez", Age: 2000, Email: "hello@world.fu", Role: "admin"})

}
