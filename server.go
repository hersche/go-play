package main

import (
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/hersche/go-play/config"
	"gitlab.com/hersche/go-play/models"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"golang.org/x/crypto/bcrypt"
)

func main() {
	configuration := config.GetConfig()
	db, err := gorm.Open("sqlite3", "test.db")
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	e := echo.New()

	e.Use(middleware.Logger())
	r := e.Group("/restricted")
	r.Use(middleware.JWT([]byte(configuration.JWT_SECRET)))
	e.GET("/test", func(c echo.Context) error {
		return c.String(http.StatusOK, "From config-json: "+configuration.DB_USERNAME)
	})
	e.GET("/users/create", func(c echo.Context) error {
		return c.String(http.StatusOK, models.CreateUser(c, db))
	})
	e.GET("/pass", func(c echo.Context) error {

		var result, err = bcrypt.GenerateFromPassword([]byte("some foo"), 6)
		if err != nil {
			panic("what a weird val")
		}
		fmt.Println()
		var testResult = bcrypt.CompareHashAndPassword(result, []byte("some foo"))
		fmt.Print("Positive result: ")
		fmt.Println(testResult)
		testResult = bcrypt.CompareHashAndPassword(result, []byte("wrong pass"))
		fmt.Print("Negative result: ")
		fmt.Println(testResult)
		return c.String(http.StatusOK, string(result))
	})
	r.GET("/users", func(c echo.Context) error {
		return c.JSON(http.StatusOK, models.ReturnJsonUsers(db))
	})
	e.GET("/users/login", func(c echo.Context) error {
		return c.String(http.StatusOK, models.LoginUser(c, db))
	})
	e.GET("/users/delete/:name", func(c echo.Context) error {
		name := c.Param("name")
		return c.String(http.StatusOK, models.DeleteUserByID(db, name))
	})
	e.GET("/users/:id", func(c echo.Context) error {
		id := c.Param("id")
		idInt, err := strconv.Atoi(id)
		if err != nil {
			panic("what a weird id")
		}
		return c.JSON(http.StatusOK, models.ReturnJsonUserByID(db, idInt))
	})

	e.GET("/users/:id/add/tag/:tag", func(c echo.Context) error {
		id := c.Param("id")
		tag := c.Param("tag")
		idInt, err := strconv.Atoi(id)
		if err != nil {
			panic("what a weird id")
		}
		models.AddTagToUserId(tag, idInt, db)
		return c.JSON(http.StatusOK, models.ReturnJsonUserByID(db, idInt))
	})

	e.Logger.Fatal(e.Start(":1323"))
}
