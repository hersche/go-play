This project simply exists to explore methods and ways how to work with golang, echo and gorm, after working with Laravel.

Start by install dependencies with

> chmod +x installDeps.sh # make it executeable  
> ./installDeps.sh

Run migrate to create the db

> go run migrate.go

Run the server

> go run server.go

Don't get confused by config.json. Sqlite is in use, but there's a config for Mysql. However, this allows play also with other db's.

The startpage is not set, but after migrate, you can go for those (all GET):

- /users/1 - or any other id, to get user-details
- /users/create - name, email, password, role are params.
- /users/login - name, password are params. Returning a JWT-token
- /restricted/users - list all users, JWT-protected
- /users/delete/:name - delete user by name
- /users/1/add/tag/aTagText - add tag by user_id
- /test - read from config
- /pass - static password-test, sum-up (in code)

A word about my motivation to play with this

I learned around various languages already and for the web, i mostly use PHP. 

As the performance in PHP is natural limited (process-based, script-language, ...) and the fastest frameworks i found now where Elixir, maybe something in Rust and, a lot of Times: echo/golang. This is also the case compared to Django, Node etc. Same time, golang can compile fluently to lot of architectures, allows also using a rpi or similiar low-end hw.

More performance is nice for better user-expirience. But the point i rather see: A project with a lot of users, written in PHP, needs, let's say 5 servers. By more performant code and environment, we can reduce the amount of servers (maybe to 2 servers or even one? or 10 projects on one server? :D). Therefore, we need less energy and less hardware - which hopefully have a small, positive influence on the climate-change-situation.

Have fun in try further things!