#!/bin/bash
go get github.com/jinzhu/gorm
go get github.com/jinzhu/gorm/dialects/sqlite
go get github.com/labstack/echo
go get github.com/labstack/echo/middleware
go get golang.org/x/crypto/bcrypt